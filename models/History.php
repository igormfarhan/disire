<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property int $id
 * @property int $sirkuler_id
 * @property int $user_id
 * @property int $document_type_id
 * @property string|null $document_scr
 * @property string $document_name
 * @property int $document_revision
 * @property int $document_status_id
 * @property string $komentar
 * @property int $date
 *
 * @property DocumentType $documentStatus
 * @property DocumentType $documentType
 * @property Sirkuler $sirkuler
 * @property User $user
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['sirkuler_id', 'user_id', 'document_type_id', 'document_name', 'document_revision', 'document_status_id', 'komentar', 'date'], 'required'],
            [['sirkuler_id', 'user_id', 'document_type_id', 'document_revision', 'document_status_id', 'date'], 'integer'],
            [['document_scr','document_name', 'komentar'], 'string', 'max' => 255],
            [['document_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::className(), 'targetAttribute' => ['document_status_id' => 'id']],
            [['document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::className(), 'targetAttribute' => ['document_type_id' => 'id']],
            [['sirkuler_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sirkuler::className(), 'targetAttribute' => ['sirkuler_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sirkuler_id' => 'Sirkuler ID',
            'user_id' => 'User ID',
            'document_type_id' => 'Document Type ID',
            'document_scr' => 'Document Name',
            'document_name' => 'File Name',
            'document_revision' => 'Document Revision',
            'document_status_id' => 'Document Status ID',
            'komentar' => 'Komentar',
            'date' => 'Date',
        ];
    }

    /**
     * Gets query for [[DocumentStatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->hasOne(DocumentStatus::className(), ['id' => 'document_status_id']);
    }

    /**
     * Gets query for [[DocumentType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentType()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'document_type_id']);
    }

    /**
     * Gets query for [[Sirkuler]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSirkuler()
    {
        return $this->hasOne(Sirkuler::className(), ['id' => 'sirkuler_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
