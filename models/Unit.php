<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property int $id
 * @property string $nama_unit
 *
 * @property JenisKontrak[] $jenisKontraks
 * @property Sirkuler[] $sirkulers
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_unit'], 'required'],
            [['nama_unit'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_unit' => 'Nama Unit',
        ];
    }

    /**
     * Gets query for [[JenisKontraks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenisKontraks()
    {
        return $this->hasMany(JenisKontrak::className(), ['unit_id' => 'id']);
    }

    /**
     * Gets query for [[Sirkulers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSirkulers()
    {
        return $this->hasMany(Sirkuler::className(), ['unit_id' => 'id']);
    }
}
