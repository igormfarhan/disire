<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sirkuler_status".
 *
 * @property int $id
 * @property string $status
 * @property string $badge
 */
class SirkulerStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sirkuler_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'badge'], 'required'],
            [['status', 'badge'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'badge' => 'Badge',
        ];
    }
}
