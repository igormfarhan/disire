<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sirkuler".
 *
 * @property int $id
 * @property string $judul_kontrak
 * @property int $unit_id_id
 * @property int $jenis_kontrak_id
 * @property int $status_id
 * @property int $submit_date
 * @property int $submit_user
 * @property int $approver_id
 */
class Sirkuler extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $dokuments;

    public static function tableName()
    {
        return 'sirkuler';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul_kontrak', 'unit_id','jenis_kontrak_id', 'submit_date', 'submit_user'], 'required'],
            [['unit_id','jenis_kontrak_id', 'status_id', 'submit_date', 'submit_user','approver_id'], 'integer'],
            [['judul_kontrak',], 'string', 'max' => 255],
            [['submit_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['submit_user' => 'id']],
            [['dokumens'], 'safe'],
            [['dokuments'], 'file','extensions'=>'pdf, doc, docx, rar, zip'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul_kontrak' => 'Judul Kontrak',
            'unit_id' => 'Unit',
            'jenis_kontrak_id' => 'Jenis Kontrak',
            'status_id' => 'Status',
            'submit_date' => 'Submit Date',
            'submit_user' => 'User',
            'approver_id' => 'Approver',
   


        ];
    }

    public function getHistories()
    {
        return $this->hasMany(History::className(), ['sirkuler_id' => 'id']);
    }

    public function getApprover()
    {
        return $this->hasOne(User::className(), ['id' => 'approver_id']);
    }

    public function getSubmitUser()
    {
        return $this->hasOne(User::className(), ['id' => 'submit_user']);
    }

    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(SirkulerStatus::className(), ['id' => 'status_id']);
    }

    public function getJenisKontrak()
    {
        return $this->hasOne(JenisKontrak::className(), ['id' => 'jenis_kontrak_id']);
    }




}
