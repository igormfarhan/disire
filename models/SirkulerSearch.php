<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sirkuler;
use Yii;

/**
 * SirkulerSearch represents the model behind the search form of `app\models\Sirkuler`.
 */
class SirkulerSearch extends Sirkuler
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'unit_id', 'jenis_kontrak_id', 'status_id', 'submit_date', 'submit_user'], 'integer'],
            [['judul_kontrak'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$query)
    {
        // $userId = Yii::$app->User->id;
        // $query = Sirkuler::find()->where(['submit_user'=> $userId]);
        // $query = Sirkuler::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'unit_id' => $this->unit_id,
            'jenis_kontrak_id' => $this->jenis_kontrak_id,
            'status_id' => $this->status_id,
            'submit_date' => $this->submit_date,
            'submit_user' => $this->submit_user,
        ]);

        $query->andFilterWhere(['like', 'judul_kontrak', $this->judul_kontrak]);

        return $dataProvider;
    }
}
