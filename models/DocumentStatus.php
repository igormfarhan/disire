<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document_status".
 *
 * @property int $id
 * @property string $status
 * @property string $badge
 *
 * @property History[] $histories
 */
class DocumentStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'badge'], 'required'],
            [['status', 'badge'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'badge' => 'Badge',
        ];
    }

    /**
     * Gets query for [[Histories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['document_status_id' => 'id']);
    }
}
