<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_kontrak".
 *
 * @property int $id
 * @property string $nama_kontrak
 * @property int $unit_id
 *
 * @property Unit $unit
 */
class JenisKontrak extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_kontrak';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_kontrak', 'unit_id'], 'required'],
            [['unit_id'], 'integer'],
            [['nama_kontrak'], 'string', 'max' => 255],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['unit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_kontrak' => 'Nama Kontrak',
            'unit_id' => 'Unit ID',
        ];
    }

    /**
     * Gets query for [[Unit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }
}
