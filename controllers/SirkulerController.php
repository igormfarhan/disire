<?php

namespace app\controllers;

use Yii;
use app\models\Sirkuler;
use app\models\SirkulerSearch;
use app\models\User;
use app\models\Unit;
use app\models\JenisKontrak;
use app\models\History;
use app\models\HistorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use app\components\AccessRule;
use yii\web\ForbiddenHttpException;
use yii\base\Model;
use aryelds\sweetalert\SweetAlert;

/**
 * SirkulerController implements the CRUD actions for Sirkuler model.
 */
class SirkulerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sirkuler models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new SirkulerSearch();        
        
        if (\Yii::$app->user->can('reviewer')) {
            $query = Sirkuler::find()->where(['status_id'=> [2,3,4,5,6,99]]);          
        } else if (\Yii::$app->user->can('user')){
            $userId = Yii::$app->User->id;
            $query = Sirkuler::find()->where(['submit_user'=> $userId]);
        } else if (\Yii::$app->user->can('approver')){
            $userId = Yii::$app->User->id;
            $query = Sirkuler::find()->where(['approver_id'=> $userId,'status_id'=>[3,4,5,6,99]]);
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$query);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sirkuler model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $history = new History();
        $count = 3;

        if ($history->load(Yii::$app->request->post())) {
            
            for ($i=1 ; $i <= $count; $i++){
                $doc = UploadedFile::getInstance($history, "document_name[$i]");
                if (!is_null($doc)) {
                    $history->sirkuler_id = $model->id;
                    $history->user_id = Yii::$app->user->getId();
                    $history->document_status_id = 2;
                    $history->document_revision = 0;
                    $history->document_type_id = $i;        
                    $history->date = time();

                    $history->document_status_id = 2;
                    $history->document_status_id = 2;
                    $history->document_scr = $doc->name;
                    $tmp = explode(".", $doc->name);
                    $ext = end($tmp);
                    // generate a unique file name to prevent duplicate filenames
                    $history->document_name = Yii::$app->security->generateRandomString(6).".{$ext}";                            
                    // the path to save file, you can set an uploadPath
                    // in Yii::$app->params (as used in example below)                       
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/sirkuler/';
                    $path = Yii::$app->params['uploadPath'] . $history->document_name;
                    $doc->saveAs($path);

                    $history->save();
                }
            }

            // $newhistory = History::find()->where(['sirkuler_id' => 82,'document_status_id'=>2]);
            $newhistory = History::find()->where(['sirkuler_id'=>$id,'document_status_id'=>2]);
            $countstatus = $newhistory->count();

            if($countstatus == $count){
                for ($y=1 ; $y <= $count; $y++){
                    $check = History::find()->where(['sirkuler_id'=>$id,'document_type_id'=> $y,'document_status_id'=>2])->one();
                    $check->document_status_id = 3;
                    $check->save();
                    $hi = $check->document_status_id;
                    $ss = Sirkuler::findOne($id);
                    $ss->status_id = 2;
                    $ss->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }


        return $this->render('view', [
            'model' => $model,
            'history' => $history,

        ]);
    }

    /**
     * Creates a new Sirkuler model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        // if(Yii::$app->user->can('create-kontrak')) { 
            //Find out how many document have been submitted by the form
            // $count = count(Yii::$app->request->post('History', []));
            $count = 9;

            //Send at least one model to the form
            $documents = [];

            //Create an array of the documents submitted
            for($i = 1; $i <= $count; $i++) {
                $documents[] = new History();
            }

            $model  = new Sirkuler();
            
    
            if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($documents, Yii::$app->request->post())) {
                
                $model->submit_user = Yii::$app->user->getId();
                $model->submit_date = time();
                $model->approver_id = 1;
                $model->status_id = 1;
                $model->save();

                $nill = 0;  
                $status = 1;              

                foreach ($documents as $index => $document){
                    $docs = UploadedFile::getInstance($document, "[$index]document_name");
                    
                    if(is_null($docs)){
                        $nill++;
                    }
                }

                if ($nill == 0){
                    $status = 3;
                    $model->status_id = 2;
                    $model->save();
                } 


                foreach ($documents as $index => $document)
                {
                    $document->sirkuler_id = $model->id;
                    $document->user_id = $model->submit_user;
                    $document->document_status_id = $status;
                    $document->document_revision = 0;
                    $document->document_type_id = $index+1;        
                    $document->date = time();

                    $doc = UploadedFile::getInstance($document, "[$index]document_name");

                        if (!is_null($doc)) {
                            $document->document_status_id = 2;
                            $document->document_status_id = 2;
                            $document->document_scr = $doc->name;
                            $tmp = explode(".", $doc->name);
                            $ext = end($tmp);
                            // generate a unique file name to prevent duplicate filenames
                            $document->document_name = Yii::$app->security->generateRandomString(6).".{$ext}";                            
                            // the path to save file, you can set an uploadPath
                            // in Yii::$app->params (as used in example below)                       
                            Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/sirkuler/';
                            $path = Yii::$app->params['uploadPath'] . $document->document_name;
                            $doc->saveAs($path);
                        }

                    $document->save();
                }
                
                if ($model->save()) {            
                    return $this->redirect(['view', 'id' => $model->id]);             
                }  else {
                    var_dump ($model->getErrors()); 
                    var_dump ($documents->getErrors());
                    
                    die();
                }
            }
                return $this->render('create', [
                'model' => $model,
                'documents' => $documents,
                ]);   
        // } else {
        //     throw new ForbiddenHttpException;
        // }
    }

    /**
     * Updates an existing Sirkuler model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        // if (!Yii::$app->user->can('update-kontrak',['id' => $model])){
        //     throw new ForbiddenHttpException;
        // }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sirkuler model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sirkuler model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sirkuler the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sirkuler::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionList($id){
        $kontraks = JenisKontrak::find()->where(['unit_id' => $id])
                                       ->orderBy('id ASC')
				                       ->all();
				
		if (!empty($kontraks)) {
			foreach($kontraks as $kontrak) {
				echo "<option value='".$kontrak->id."'>".$kontrak->nama_kontrak."</option>";
			}
		} else {
			echo "<option>-</option>";
		}
    }

    public function actionReview($id){
        
        if (Yii::$app->user->can('reviewer')){
            echo SweetAlert::widget([
                'options' => [
                    'title' => "Are you sure?",
                    'text' => "You will review this contract",
                    'type' => SweetAlert::TYPE_WARNING,
                    'showCancelButton' => true,
                    'confirmButtonColor' => "#DD6B55",
                    'confirmButtonText' => "Yes",
                    'cancelButtonText' => "No",
                    'closeOnConfirm' => false,
                    'closeOnCancel' => false
                ],
            ]);
            $model = $this->findModel($id);
            $model->status_id = 3;
            $model->save();
        } else {
            throw new ForbiddenHttpException;
        }

        return $this->redirect(['view', 'id' => $model->id]);

    }

    public function actionApprove($id){

        $model = $this->findModel($id);
        
        if (Yii::$app->user->can('approver')){            
            
            $model->status_id = 5;
            $model->save();
        } else {
            echo SweetAlert::widget([
                'options' => [
                    'title' => "Sorry!!",
                    'text' => "Only Approver can do this Action",
                    'type' => SweetAlert::TYPE_ERROR
                ]
              ]);
            throw new ForbiddenHttpException;
        }


        return $this->redirect(['view', 'id' => $model->id]);

    }

    public function actionComplete($id){
        
        if (Yii::$app->user->can('approver')){
            $model = $this->findModel($id);
            $model->status_id = 7;
            $model->save();
        } else {
            throw new ForbiddenHttpException;
        }

        return $this->redirect(['view', 'id' => $model->id]);

    }

    public function actionDecline($id){
        
        if (Yii::$app->user->can('approver')){
            $model = $this->findModel($id);
            $model->status_id = 99;
            $model->save();
        } else {
            throw new ForbiddenHttpException;
        }

        return $this->redirect(['view', 'id' => $model->id]);

    }

    public function actionApprovedoc($id,$type){

        $history = History::find()->where(['sirkuler_id'=>$id,'document_type_id'=>$type])->orderBy(['id' => SORT_DESC])->one();
        $history->document_status_id = 6;
        $history->save();

        return $this->redirect(['sirkuler/view', 'id' => $history->sirkuler_id]);

    }

    public function actionRevisedoc($id,$type){

        $history = History::find()->where(['sirkuler_id'=>$id,'document_type_id'=>$type])->orderBy(['id' => SORT_DESC])->one();
        $history->document_status_id = 5;
        $history->save();

        $sirkuler = Sirkuler::findOne($id);
        $sirkuler->status_id = 3;
        $sirkuler->save();

        return $this->redirect(['sirkuler/view', 'id' => $history->sirkuler_id]);

    }    

    public function actionUpdatedoc($id){



        return $this->redirect(['history/update', 'id' => $id]);

    } 

    public function actionDeletedoc($id){

        $history = History::findOne($id);
        $idx = $history->sirkuler_id;
        $history->delete();

        return $this->redirect(['sirkuler/view', 'id' => $idx ]);

    }


}
