<?= $form->field($model, 'unit')->dropDownList(Yii::$app->helper->listUnit(),['prompt'=>'Pilih Unit']); ?>

<?= $form->field($model, 'lokasi_kerjasama')->dropDownList(Yii::$app->helper->listLokasiKerjasama(),['prompt'=>'Pilih Lokasi Kerjasama']); ?>

<?= $form->field($model, 'jenis_kontrak_id')->dropDownList(Yii::$app->helper->listJenisKontrak(),['prompt'=>'Pilih Jenis Kontrak']); ?>

<?= $form->field($model2, 'dokumen1')->widget(FileInput::classname(), [
            'pluginOptions'=> $pluginOption,
            'name' => 'attachment_1',
          ]);   
    ?>

    <?= $form->field($model2, 'dokumen2')->widget(FileInput::classname(), [
            'pluginOptions'=> $pluginOption,
            'name' => 'attachment_2',
          ]);   
    ?>   

    <?= $form->field($model2, 'dokumen3')->widget(FileInput::classname(), [
            'pluginOptions'=> $pluginOption,
            'name' => 'attachment_3',
          ]);   
    ?>  

<?php 

echo $form->field($document, "[$index]document_name")->widget(FileInput::classname(), [
          'pluginOptions'=> $pluginOption,
          'name' => 'attachment_1',
        ]);
