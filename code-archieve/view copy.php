<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\Sirkuler;

/* @var $this yii\web\View */
/* @var $model app\models\Sirkuler */

$pluginOption = [
    'allowedFileExtensions'=>['pdf','doc','docx','zip'],
    'showUpload' => true,
    'showPreview' => false,
    'showCaption' => true,
    'showRemove' => true,
    'browseLabel' =>  'Silahkan upload dokumen yang sudah direvisi',
];

$this->title = $model->judul_kontrak;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Sirkuler Kontrak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>

<div class="sirkuler-view">
    <div class="box box-success">
        <div class="box-header with-border">
            <div class="box-tools pull-left">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'judul_kontrak',
                [
                    'attribute' => 'unit',
                    'value' => Yii::$app->helper->getUnit($model->unit_id),
                ],
                'submit_date:datetime',
                'submitUser.name',
                'submitUser.email',
                
            ],
        ]) ?>
        </div>
    </div>
</div>

<div class="dokumen-view">
  <div class="panel box box-primary">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Perjanjian Kerja Sama
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="box-body">
        <table class="table table-bordered">
          <tr>
            <th style="width: 300px">Nama File</th>
            <th>Status</th>
            <th>Komentar</th>
          </tr>
          <tr>
            <td>Update</td>
            <td><span class="badge bg-light-blue">Sedang di review</span></td>
            <td>Tanggal pengajuan tidak sesuai</td>
          </tr>
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
        <div class="pagination pagination-sm no-margin pull-right">
          <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>  
          <?= $form->field($model, 'id')
                   ->label(false)
                   ->widget(FileInput::classname(), [
                                                      'pluginOptions'=> $pluginOption,
                                                      'name' => 'attachment_1',
                                                    ]
                    );   
          ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?php
          $model2 = Sirkuler::find()->where(['id'=> $model->id])->one();
          echo \yii2mod\comments\widgets\Comment::widget(['model' => $model2,]); ?>
      </div>
    </div>
  </div>  
</div>

<div class="dokumen-view">
  <div class="panel box box-primary">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Perjanjian Kerja Sama
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="box-body">
        <table class="table table-bordered">
          <tr>
            <th style="width: 300px">Nama File</th>
            <th>Status</th>
            <th>Komentar</th>
          </tr>
          <tr>
            <td>Update</td>
            <td><span class="badge bg-light-blue">Sedang di review</span></td>
            <td>Tanggal pengajuan tidak sesuai</td>
          </tr>
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
        <div class="pagination pagination-sm no-margin pull-right">
          <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>  
          <?= $form->field($model, 'id')
                   ->label(false)
                   ->widget(FileInput::classname(), [
                                                      'pluginOptions'=> $pluginOption,
                                                      'name' => 'attachment_1',
                                                    ]
                    );   
          ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?php
          $model2 = Sirkuler::find()->where(['id'=> $model->id])->one();
          echo \yii2mod\comments\widgets\Comment::widget(['model' => $model2,]); ?>
      </div>
    </div>
  </div>  
</div>








