<?php
namespace app\components;

use Yii;
use yii\base\Component;

class helper extends Component{
    private $unit = [
        '1' => 'BGES', // 1, 2, 7
        '2' => 'Consumer Marketing', // 3, 4, 7
        '3' => 'Logistik', // 5, 6, 7
        '4' => 'Witel Bandung', // 1, 2, 3, 4, 5
        '5' => 'Witel Bandung Barat',
        '6' => 'Witel Karawang',
        '7' => 'Witel Sukabumi',
        '8' => 'Witel Tasikmalaya',
        '9' => 'Witel Cirebon',
        '10' => 'Unit Lainnya',
    ];
    // private $lokasiKerjasama = [
    //     '1' => 'Bandung',
    //     '2' => 'Bandung Barat',
    //     '3' => 'Karawang',
    //     '4' => 'Sukabumi',
    //     '5' => 'Tasikmalaya',
    //     '6' => 'Cirebon',
    // ];
    private $jenisKontrak = [
        '1' => 'Kontrak Layanan',
        '2' => 'Kontrak Berlangganan',
        '3' => 'Marketing Partnership',
        '4' => 'Dealership Indihome',
        '5' => 'Kontrak Harga Satuan',
        '6' => 'Surat Pesanan',
        '7' => 'Kontrak Lainnya',
    ];

    private $status = [
        '1' => 'Menunggu dokumen lengkap',
        '2' => 'Sedang diulas',
        '3' => 'Terdapat dokumen yang perlu direvisi',
        '4' => 'Kontrak disetujui',
        '5' => 'Kontrak tidak disetujui',
    ];

    private $statusDokumen = [
        '1' => 'Belum diunggah',
        '2' => 'Sedang diulas',
        '3' => 'Terdapat dokumen yang perlu direvisi',
        '4' => 'Revisi dibutuhkan',
        '5' => 'Dokumen disetujui',
    ];

    public function listUnit()
    {
        return $this->unit;
    }
    public function listLokasiKerjasama()
    {
        return $this->lokasiKerjasama;
    }
    public function listJenisKontrak()
    {
        return $this->jenisKontrak;
    }
    public function listStatus()
    {
        return $this->status;
    }
    public function listStatusDokumen()
    {
        return $this->statusDokumen;
    }

    public function getUnit($id)
    {
        return $this->unit[$id];
    }
    public function getLokasiKerjasama($id)
    {
        return $this->lokasiKerjasama[$id];
    }
    public function getJenisKontrak($id)
    {
        return $this->jenisKontrak[$id];
    }
    public function getStatus($id)
    {
        return $this->status[$id];
    }
    public function getStatusDokumen($id)
    {
        return $this->statusDokumen[$id];
    }
}