<?php

namespace app\components;

use yii\rbac\Rule;
use app\models\History;
use app\models\User;

/**
 * Checks if authorID matches user passed via params
 */
class DocSubmitter extends Rule
{
    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
    // paramater $params dikirim dari AccessControl dengan nilai dari $_GET
        return isset($params['id']) ? $params['id']->submit_user == $user : false;
        // $model = History::findOne($params['id']);
        // return $model && $user->id == $model->user_id;
    }
}
?>