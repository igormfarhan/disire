<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use \app\models\Unit;
use \app\models\JenisKontrak;
use \app\models\DocumentType;
use dektrium\user\models\User;


$unit = ArrayHelper::map(Unit::find()->asArray()->all(), 'id', 'nama_unit');
$kontrak = ArrayHelper::map(JenisKontrak::find()->asArray()->all(), 'id', 'nama_kontrak');
$approvers = Yii::$app->authManager->getUserIdsByRole('approver');
$namaApprovers = ArrayHelper::map(User::findAll($approvers), 'id', 'name');

/* @var $this yii\web\View */
/* @var $model app\models\Sirkuler */
/* @var $form yii\widgets\ActiveForm */

$pluginOption = [
                    'allowedFileExtensions'=>['pdf','doc','docx','zip'],
                    'showUpload' => false,
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
];

?>

<div class="sirkuler-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'judul_kontrak')->textInput(['maxlength' => true,]) ?>

    <?= $form->field($model,'unit_id')->dropDownList($unit,
      [
        'prompt'=>'Silahkan pilih Unit',
        'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl('sirkuler/list?id=').'"+$(this).val(), function( data ) {
        $( "select#kontrak" ).html( data );
      });
      ']); 
    ?>

    <?= $form->field($model,'jenis_kontrak_id')->dropDownList($kontrak,
      [
        'prompt'=>'Silahkan pilih Jenis Kontrak',
        'id'=>'kontrak',
      ]); 
    ?>    

    <?php 
      if(Yii::$app->user->can('reviewer')){ 

        echo $form->field($model,'approver_id')->dropDownList($namaApprovers,
          [
            'prompt'=>'Silahkan pilih approver',
          ]); 
      }
    ?>   
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
