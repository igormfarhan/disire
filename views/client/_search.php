<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SirkulerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sirkuler-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'judul_kontrak') ?>

    <?= $form->field($model, 'unit') ?>

    <?= $form->field($model, 'lokasi_kerjasama') ?>

    <?= $form->field($model, 'jenis_kontrak_id') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'submit_date') ?>

    <?php // echo $form->field($model, 'submit_time') ?>

    <?php // echo $form->field($model, 'submit_user') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
