<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sirkuler */

$this->title = 'Update : ' . $model->judul_kontrak;
$this->params['breadcrumbs'][] = ['label' => 'Sirkuler', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->judul_kontrak, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="sirkuler-update">
    <div class="box box-success">
        <div class="box-header with-border">
            
        </div>
        <div class="box-body">
            <?= $this->render('_formUpdate', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>