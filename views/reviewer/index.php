<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SirkulerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Sirkuler Kontrak';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sirkuler-index">
    <div class="box box-success">        
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'submit_user',
                        'value'=>'submitUser.name', //relation name with their attribute
                    ],
                    'judul_kontrak',
                    [
                        'attribute' => 'unit_id',
                        'value'=>'unit.nama_unit', //relation name with their attribute
                    ],
                    [
                        'attribute' => 'status_id',
                        'format' => 'raw',
                        'label' => 'Status',
                        'value'=> function ($model) {
                            return Html::tag('p', $model->status->status,['class' => $model->status->badge]);
                          }
                    ],                    
                    'submit_date:date',                    
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'contentOptions' => function ($model, $key, $index, $column) {
                            return ['style' => 'min-width:150px'];
                        },
                        'buttons' => [
                            'view' => function ($url,$model,$key) {
                                if(Yii::$app->user->can('reviewer') && $model->status_id == 2){

                                    return Html::a('<span class="glyphicon glyphicon-search"></span>', ['review','id'=>$key], ['class'=>'btn btn-primary']);
                                } else {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['class'=>'btn btn-primary']);
                                }
                            },
                            'update' => function ($url,$model) {
                                return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['class'=>'btn btn-warning']);
                            },
                            'delete' => function ($url,$model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, 
                                    ['data-method' => 'post','class'=>'btn btn-danger','data-confirm'=> 'Are you sure?'],
                                    );
                                
                            },                    
                        ],
                    ],
                ],
            ]); ?>
        </div>

        


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
</div>
