<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use kartik\file\FileInput;
use app\models\Sirkuler;
use app\models\History;
use app\models\HistorySearch;
use yii2mod\alert\Alert;
use aryelds\sweetalert\SweetAlert;

echo SweetAlert::widget();

$this->registerCssFile("@web/css/progress.css");


/* @var $this yii\web\View */
/* @var $model app\models\Sirkuler */

$pluginOption = [
    'allowedFileExtensions'=>['pdf','doc','docx','zip'],
    'showUpload' => false,
    'showPreview' => false,
    'showCaption' => true,
    'showRemove' => true,
    'browseLabel' =>  'Silahkan upload dokumen yang sudah direvisi',
];

$this->title = $model->judul_kontrak;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Sirkuler Kontrak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<?php   
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js");


$js = <<< JS
var i;
var j = $model->status_id;

for (i=1; i <= j; i++){
  $('.progress2 .circle2:nth-of-type(' + i + ')').addClass('active');  
  $('.progress2 .circle2:nth-of-type(' + (i-1) + ')').removeClass('active').addClass('done');  
  $('.progress2 .circle2:nth-of-type(' + (i-1) + ') .label2').html('&#10003;');  
  $('.progress2 .bar2:nth-of-type(' + (i-1) + ')').addClass('active');  
  $('.progress2 .bar2:nth-of-type(' + (i-2) + ')').removeClass('active').addClass('done'); 

  if (i==0) {
    $('.progress2 .bar2').removeClass().addClass('bar2');
    $('.progress2 div.circle2').removeClass().addClass('circle2');
    i = 1;
  }
}
JS;
$this->registerJs($js);


?>
<!-- partial:index.partial.html -->
<div class="progress2">
  <div class="circle2">
    <span class="label2">1</span>
    <span class="title2">Upload Berkas</span>
  </div>
  <span class="bar2 "></span>
  <div class="circle2">
    <span class="label2">2</span>
    <span class="title2">Tunggu Review</span>
  </div>
  <span class="bar2"></span>
  <div class="circle2">
    <span class="label2">3</span>
    <span class="title2">Sedang Direview</span>
  </div>
  <span class="bar2"></span>
  <div class="circle2">
    <span class="label2">4</span>
    <span class="title2">Revisi Diperlukan</span>
  </div>
  <span class="bar2"></span>
  <div class="circle2">
    <span class="label2">5</span>
    <span class="title2">Kontrak Disetujui</span>
  </div>
  <span class="bar2"></span>
  <div class="circle2">
    <span class="label2">6</span>
    <span class="title2">Sirkuler Selesai</span>
  </div>
</div>
<!-- partial -->

<div class="sirkuler-view">
    <div class="box box-success">
        <div class="box-header with-border">
            <div class="box-tools pull-left">
                <?php

                  if($model->status_id < 4){
                    echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                  }

                  echo Html::a('Delete', ['delete', 'id' => $model->id], [
                      'class' => 'btn btn-danger',
                      'data' => [
                          'confirm' => 'Are you sure you want to delete this item?',
                          'method' => 'post',
                    ],
                  ]); 
                ?>
            </div>
            <div class="box-tools pull-right">
                <?php

                  if(Yii::$app->user->can('approver') && $model->status_id < 4){
                  
                   echo Html::a('Approve this contract', ['approve', 'id' => $model->id], [
                      'class' => 'btn btn-success',
                      'data' => [
                          'confirm' => 'Are you sure you want to approve this contract?',
                          'method' => 'post',
                      ],
                    ]);
                    echo Html::a('Decline', ['decline', 'id' => $model->id], [
                      'class' => 'btn btn-danger',
                      'data' => [
                          'confirm' => 'Are you sure you want to decline this contract?',
                          'method' => 'post',
                      ],
                    ]);
                  } else if (Yii::$app->user->can('approver') && $model->status_id == 5){
                    echo Html::a('Complete', ['complete', 'id' => $model->id], [
                      'class' => 'btn btn-success',
                      'data' => [
                          'confirm' => 'Are you sure you want to complete this contract?',
                          'method' => 'post',
                      ],
                    ]); 
                  }
                               
             ?>
            </div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'submit_date:datetime',
                'judul_kontrak',
                'unit.nama_unit', 
                'jenisKontrak.nama_kontrak',                
                [
                  'attribute' => 'submit_user',
                  'format' => 'raw',
                  'label' => 'User',
                  'value' => $model->submitUser->name,
                ],
                'submitUser.email:email',
                [
                  'attribute' => 'approver_id',
                  'format' => 'raw',
                  'label' => 'Approver',
                  'value' => $model->approver->name,
                ],             
            ],
        ]) ?>
        </div>
    </div>
</div>

<div class="pks">
  <div class="panel box box-primary">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Draft Perjanjian Kerja Sama
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="box-body">
      
      <?php

        $searchModel = new HistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,1);
        
        echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'sirkuler_id',
                    'date:datetime',
                    [
                      'attribute' => 'document_name',
                      'format' => 'raw',
                      'label' => 'Nama File',
                      'value' => function ($model) {
                        return Html::a($model->document_scr, ['@web/uploads/sirkuler/'.$model->document_name]);
                      }
                    ],
                    [
                      'attribute' => 'user_id',
                      'format' => 'raw',
                      'label' => 'User',
                      'value' => 'user.name',
                    ],
                    // 'documentStatus.status'
                    [
                      'attribute' => 'document_status_id',
                      'format' => 'raw',
                      'label' => 'Status',
                      'value' => function ($model) {
                        return Html::tag('p', $model->documentStatus->status,['class' => $model->documentStatus->badge]);
                      }
                    ],
                    'komentar',
                    [
                      'class' => 'yii\grid\ActionColumn',
                      'template' => '{deletedoc}', // {updatedoc} 
                      'buttons' => [
                          'updatedoc' => function ($url,$history,$key) { 
                              return Html::a('<span class="glyphicon glyphicon-edit"></span>',$url , ['class'=>'btn btn-primary']);
                          },
                          'deletedoc' => function ($url,$history) {
                              return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, 
                                  ['data-method' => 'post','class'=>'btn btn-danger','data-confirm'=> 'Are you sure?'],
                          );
                              
                          } ,                    
                      ],
                  ],
                ],
            ]); ?>
        
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
        <div class="pagination pagination-sm no-margin pull-right">
          
          <?php 
            if(!Yii::$app->user->can('approver')){
              $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);
              echo $form->field($history, 'document_name[1]')
                      ->label(false)
                      ->widget(FileInput::classname(), [
                                                          'pluginOptions'=> $pluginOption,
                                                          'name' => 'attachment_1',
                                                        ]
                        );   

              echo $form->field($history, 'komentar')->textArea(['maxlength' => true]); 
            }
          ?>
        </div>

        <div class="form-group">
          <?php
            if(!Yii::$app->user->can('approver')){
              echo Html::submitButton('Save', ['class' => 'btn btn-success']);  
              ActiveForm::end();            
            }
          ?>
          <div class="box-tools pull-right">
                <?= Html::a('Approve this document', ['approvedoc', 'id' => $model->id, 'type'=>1], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => 'Are you sure you want to approve this document?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Revise', ['revisedoc', 'id' => $model->id, 'type'=>1], [
                    'class' => 'btn btn-warning',
                    'data' => [
                        'confirm' => 'Are you sure you want to approve this document?',
                        'method' => 'post',
                    ],
                ]) ?>
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>

<div class="aktapendirian">
  <div class="panel box box-primary">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Akta Pendirian
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="box-body">
      
      <?php

        $searchModel = new HistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,2);
        
        echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'sirkuler_id',
                    'date:datetime',
                    [
                      'attribute' => 'document_name',
                      'format' => 'raw',
                      'label' => 'Nama File',
                      'value' => function ($model) {
                        return Html::a($model->document_scr, ['@web/uploads/sirkuler/'.$model->document_name]);
                      }
                    ],
                    [
                      'attribute' => 'user_id',
                      'format' => 'raw',
                      'label' => 'User',
                      'value' => 'user.name',
                    ],
                    // 'documentStatus.status'
                    [
                      'attribute' => 'document_status_id',
                      'format' => 'raw',
                      'label' => 'Status',
                      'value' => function ($model) {
                        return Html::tag('p', $model->documentStatus->status,['class' => $model->documentStatus->badge]);
                      }
                    ],
                    'komentar',
                    [
                      'class' => 'yii\grid\ActionColumn',
                      'template' => '{deletedoc}', // {updatedoc} 
                      'buttons' => [
                          'updatedoc' => function ($url,$history,$key) { 
                              return Html::a('<span class="glyphicon glyphicon-edit"></span>',$url , ['class'=>'btn btn-primary']);
                          },
                          'deletedoc' => function ($url,$history) {
                              return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, 
                                  ['data-method' => 'post','class'=>'btn btn-danger','data-confirm'=> 'Are you sure?'],
                          );
                              
                          } ,                    
                      ],
                  ],
                ],
            ]); ?>
        
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
        <div class="pagination pagination-sm no-margin pull-right">
          
          <?php 
            if(!Yii::$app->user->can('approver')){
              $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);
              echo $form->field($history, 'document_name[2]')
                      ->label(false)
                      ->widget(FileInput::classname(), [
                                                          'pluginOptions'=> $pluginOption,
                                                          'name' => 'attachment_2',
                                                        ]
                        );   

              echo $form->field($history, 'komentar')->textArea(['maxlength' => true]); 
            }
          ?>
        </div>

        <div class="form-group">
          <?php
            if(!Yii::$app->user->can('approver')){
              echo Html::submitButton('Save', ['class' => 'btn btn-success']);  
              ActiveForm::end();            
            }
          ?>
          <div class="box-tools pull-right">
                <?= Html::a('Approve this document', ['approvedoc', 'id' => $model->id, 'type'=>2], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => 'Are you sure you want to approve this document?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Revise', ['revisedoc', 'id' => $model->id, 'type'=>2], [
                    'class' => 'btn btn-warning',
                    'data' => [
                        'confirm' => 'This document need to be revised?',
                        'method' => 'post',
                    ],
                ]) ?>
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>

<div class="aktaterbaru">
  <div class="panel box box-primary">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Akta Terbaru
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="box-body">
      
      <?php

        $searchModel = new HistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,3);
        
        echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'sirkuler_id',
                    'date:datetime',
                    [
                      'attribute' => 'document_name',
                      'format' => 'raw',
                      'label' => 'Nama File',
                      'value' => function ($model) {
                        return Html::a($model->document_scr, ['@web/uploads/sirkuler/'.$model->document_name]);
                      }
                    ],
                    [
                      'attribute' => 'user_id',
                      'format' => 'raw',
                      'label' => 'User',
                      'value' => 'user.name',
                    ],
                    // 'documentStatus.status'
                    [
                      'attribute' => 'document_status_id',
                      'format' => 'raw',
                      'label' => 'Status',
                      'value' => function ($model) {
                        return Html::tag('p', $model->documentStatus->status,['class' => $model->documentStatus->badge]);
                      }
                    ],
                    'komentar',
                    [
                      'class' => 'yii\grid\ActionColumn',
                      'template' => '{deletedoc}', // {updatedoc} 
                      'buttons' => [
                          'updatedoc' => function ($url,$history,$key) { 
                              return Html::a('<span class="glyphicon glyphicon-edit"></span>',$url , ['class'=>'btn btn-primary']);
                          },
                          'deletedoc' => function ($url,$history) {
                              return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, 
                                  ['data-method' => 'post','class'=>'btn btn-danger','data-confirm'=> 'Are you sure?'],
                          );
                              
                          } ,                    
                      ],
                  ],
                ],
            ]); ?>
        
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
        <div class="pagination pagination-sm no-margin pull-right">
          
          <?php 
            if(!Yii::$app->user->can('approver')){
              $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);
              echo $form->field($history, 'document_name[3]')
                      ->label(false)
                      ->widget(FileInput::classname(), [
                                                          'pluginOptions'=> $pluginOption,
                                                          'name' => 'attachment_3',
                                                        ]
                        );   

              echo $form->field($history, 'komentar')->textArea(['maxlength' => true]); 
            }
          ?>
        </div>

        <div class="form-group">
          <?php
            if(!Yii::$app->user->can('approver')){
              echo Html::submitButton('Save', ['class' => 'btn btn-success']);  
              ActiveForm::end();            
            }
          ?>
          <div class="box-tools pull-right">
                <?= Html::a('Approve this document', ['approvedoc', 'id' => $model->id, 'type'=>3], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => 'Are you sure you want to approve this document?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Revise', ['revisedoc', 'id' => $model->id, 'type'=>3], [
                    'class' => 'btn btn-warning',
                    'data' => [
                        'confirm' => 'This document need to be revised?',
                        'method' => 'post',
                    ],
                ]) ?>
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>


<div class="comment">
  <div class="panel box box-primary">
    
    
      <div class="box-body">
        <?php
            $model2 = Sirkuler::find()->where(['id'=> $model->id])->one();
            echo \yii2mod\comments\widgets\Comment::widget(['model' => $model2,]); 
        ?>        
      </div><!-- /.box-body -->
      
        
        
    
  </div>  
</div>








