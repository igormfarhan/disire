<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HistorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="history-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sirkuler_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'document_type_id') ?>

    <?= $form->field($model, 'document_scr') ?>

    <?php // echo $form->field($model, 'document_name') ?>

    <?php // echo $form->field($model, 'document_revision') ?>

    <?php // echo $form->field($model, 'document_status_id') ?>

    <?php // echo $form->field($model, 'komentar') ?>

    <?php // echo $form->field($model, 'date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
