<?php
use yii\helpers\Html;
use yii\helpers\Url;
use mdm\admin\components\MenuHelper;
use mdm\admin\components\Helper;
use yii\bootstrap\Nav;


/* @var $this \yii\web\View */
/* @var $content string */


?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?= Html::img('@web/uploads/avatar/'.Yii::$app->User->identity->avatar, 
                    ['alt' => 'avatar','class'=>'img-circle']) 
                ?>
                <!-- <img src="<?= $directoryAsset ?>/img/user3-128x128.jpg" class="img-circle" alt="User Image"/> -->
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->User->identity->name ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        

        <?php

            $menuItems = [
                ['label' => 'Application List', 'options' => ['class' => 'header']],
                ['label' => 'Dashboard','icon' => ' fa fa-dashboard','url' => ['/client/home']],
                ['label' => 'Dashboard','icon' => ' fa fa-dashboard','url' => ['/reviewer/home']],
                ['label' => 'Dashboard','icon' => ' fa fa-dashboard','url' => ['/approver/home']],

                ['label' => 'Daftar Kontrak','icon' => ' fa-file-text','url' => ['/client/index']],
                ['label' => 'Daftar Kontrak','icon' => ' fa-file-text','url' => ['/reviewer/index']],
                ['label' => 'Daftar Kontrak','icon' => ' fa-file-text','url' => ['/approver/index']],

                ['label' => 'Buat Kontrak','icon' => ' fa-plus-circle','url' => ['/client/create']],

                ['label' => 'Debug','icon' => ' fa-bug','url' => ['/debug']],
                ['label' => 'RBAC','icon' => ' fa-user','url' => ['/admin']],
                
            ];

            $menuItems = Helper::filter($menuItems);

            echo dmstr\widgets\Menu::widget([
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems,
            ]);

        ?>

        <?php
        
        // dmstr\widgets\Menu::widget(
            // [
            //     'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
            //     'items' => [
            //         ['label' => 'Application List', 'options' => ['class' => 'header']],
            //         ['label' => 'Dashboard', 'icon' => ' fa fa-dashboard', 'url' => ['/site']],
            //         ['label' => 'Daftar Kontrak', 'icon' => ' fa-file-text', 'url' => ['/sirkuler']],
            //         ['label' => 'Buat Kontrak Baru', 'icon' => ' fa-plus-circle', 'url' => ['/sirkuler/create']],
                    // ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    // Yii::$app->user->isGuest ?
                    //     ['label' => 'Sign in', 'url' => ['/user/security/login']] :
                    // [
                    //     'label' => 'Some tools',
                    //     'icon' => 'share',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                    //         ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                    //         [
                    //             'label' => 'Level One',
                    //             'icon' => 'circle-o',
                    //             'url' => '#',
                    //             'items' => [
                    //                 ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                    //                 [
                    //                     'label' => 'Level Two',
                    //                     'icon' => 'circle-o',
                    //                     'url' => '#',
                    //                     'items' => [
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                     ],
                    //                 ],
                    //             ],
                    //         ],
                            
                    //     ],
                    // ],
                    
        //         ],
        //     ]
        // ) 
        ?>

    </section>

</aside>
