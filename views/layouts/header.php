<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->

                <!-- User Account: style can be found in dropdown.less -->
                <li class="user user-menu">
                <a href="<?= Url::toRoute('/user/settings/account');?>">
                        <?= Html::img('@web/uploads/avatar/'.Yii::$app->User->identity->avatar, 
                            ['alt' => 'avatar','class'=>'user-image']) 
                        ?>
                        <!-- <img src="<?= $directoryAsset ?>/img/user3-128x128.jpg" class="user-image" alt="User Image"/> -->
                        <span class="hidden-xs"><?= Yii::$app->User->identity->name ?></span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li>

                <li class="logout" style="margin-right: 50px">
                    <?= Html::a('Logout', ['/user/security/logout'],['data-method' => 'post']) ?>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    
                </li>
            </ul>
        </div>
    </nav>
</header>
