<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use \app\models\Unit;
use \app\models\JenisKontrak;
use \app\models\DocumentType;
use dektrium\user\models\User;


$unit = ArrayHelper::map(Unit::find()->asArray()->all(), 'id', 'nama_unit');
$kontrak = ArrayHelper::map(JenisKontrak::find()->asArray()->all(), 'id', 'nama_kontrak');
$approvers = Yii::$app->authManager->getUserIdsByRole('approver');
$namaApprovers = ArrayHelper::map(User::findAll($approvers), 'id', 'name');






/* @var $this yii\web\View */
/* @var $model app\models\Sirkuler */
/* @var $form yii\widgets\ActiveForm */

$pluginOption = [
                    'allowedFileExtensions'=>['pdf','doc','docx','zip'],
                    'showUpload' => false,
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                ]
?>

<div class="sirkuler-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'judul_kontrak')->textInput(['maxlength' => true,]) ?>

    <?= $form->field($model,'unit_id')->dropDownList($unit,
      [
        'prompt'=>'Silahkan pilih Unit',
        'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl('sirkuler/list?id=').'"+$(this).val(), function( data ) {
        $( "select#kontrak" ).html( data );
      });
      ']); 
    ?>

    <?= $form->field($model,'jenis_kontrak_id')->dropDownList($kontrak,
      [
        'prompt'=>'Silahkan pilih Jenis Kontrak',
        'id'=>'kontrak',
      ]); 
    ?>    

    <?php 
      
      echo $form->field($model,'approver_id')->dropDownList($namaApprovers,
        [
          'prompt'=>'Silahkan pilih approver',
        ]); 
    ?>   

    <?php
    foreach ($documents as $index => $document) {
      
      $doctype = DocumentType::findOne($index+1);
      $label = $doctype->type;

      echo $form->field($document, "[$index]document_name")->widget(FileInput::classname(), [
        'pluginOptions'=> $pluginOption,
      ])->label($label);
    }
    ?>

    <?php //echo $form->field($documents, "document_name")->widget(FileInput::classname(), [
       // 'pluginOptions'=> $pluginOption,
      //]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
