<?php

/* @var $this yii\web\View */

use app\models\Sirkuler;

$this->title = 'Dashboard';

?>
      <!-- Small boxes (Stat box) -->
<div class="row">
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner text-center">
        <h3>
        <?php
          echo Sirkuler::find()->where(['status_id'=>[1,2]])
                                ->andWhere(['submit_user'=>Yii::$app->user->identity->id])
                                ->count();              
        ?>
        </h3>
        <h4>Submitted</h4>
      </div>
      <div class="icon">
        <i class="fa fa-chevron-circle-up"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner text-center">
        <h3>
        <?php
          echo Sirkuler::find()->where(['status_id'=>[3]])
                                ->andWhere(['submit_user'=>Yii::$app->user->identity->id])
                                ->count();              
        ?>              
        </h3>
        <h4>Under Review</h4>
      </div>
      <div class="icon">
        <i class="fa fa-question-circle"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner text-center">
        <h3>
        <?php
          echo Sirkuler::find()->where(['status_id'=>[4,5,6]])
                                ->andWhere(['submit_user'=>Yii::$app->user->identity->id])
                                ->count();              
        ?>    
        </h3>
        <h4>Approved</h4>
      </div>
      <div class="icon">
        <i class="fa fa-check-circle"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
      <div class="inner text-center">
        <h3>
        <?php
          echo Sirkuler::find()->where(['status_id'=>99])
                                ->andWhere(['submit_user'=>Yii::$app->user->identity->id])
                                ->count();              
        ?>    
        </h3>
        <h4>Declined</h4>
      </div>
      <div class="icon">
        <i class="fa fa-times-circle"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
</div>

      
