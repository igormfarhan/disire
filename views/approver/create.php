<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sirkuler */

$this->title = 'Buat Kontrak Baru';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Sirkuler Kontrak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sirkuler-create">
    <div class="box box-success">
        <div class="box-header with-border">
            
        </div>
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
                'documents' => $documents,
            ]) ?>
        </div>
    </div>
</div>
