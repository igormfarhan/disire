<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';
$this->registerJs("jQuery('#reveal-password').change(function(){jQuery('#loginform-password').attr('type',this.checked?'text':'password');})");


$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
     
    <!-- /.login-logo -->
    <div class="login-box-body" style="width: 390px;">
        <div class="text-center">
            <?= Html::img('@web/img/telkom.png', ['alt'=>'Telkom', 'style'=>'width: 50%; margin-bottom: 15px']);?>
        </div>
        <h3 class="login-box-msg">Digital Sirkuler Review</h3>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('nomor induk karyawan')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>

        <!-- <div class="row">
            <div class="col-xs-6">
                <?= Html::checkbox('reveal-password', false, ['id' => 'reveal-password']) ?> <?= Html::label('Show password', 'reveal-password') ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
        </div> -->

        <?= Html::submitButton('Sign in', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
 

        <?php ActiveForm::end(); ?>

        

        <div class="text-center" style="padding-top:10px">
            <?= Html::a('Sign up', ['/controller/action'], ['class'=>'btn btn-default btn-block btn-flat']) ?>
            <?= Html::a('Forgot password', ['/controller/action'], ['class'=>'btn btn-default btn-block btn-flat']) ?>
            
        </div>

        

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
