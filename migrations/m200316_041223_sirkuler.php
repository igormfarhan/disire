<?php

use yii\db\Migration;

/**
 * Class m200316_041223_sirkuler
 */
class m200316_041223_sirkuler extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%sirkuler}}', [
            'id'                   => $this->primaryKey(),
            'judul_kontrak'        => $this->string(255)->notNull(),
            'unit'                 => $this->smallInteger()->notNull(),
            'lokasi_kerjasama'     => $this->smallInteger()->notNull(),
            'jenis_kontrak_id'     => $this->smallInteger()->notNull(),
            'status'               => $this->smallInteger()->notNull(),
            'submit_date'          => $this->integer()->notNull(),
            'submit_time'          => $this->integer()->notNull(),
            'submit_user'          => $this->smallInteger()->notNull(),

        ]);

        // $this->addForeignKey('{{%fk_user_profile}}', '{{%profile}}', 'user_id', '{{%user}}', 'id', $this->cascade, $this->restrict);
    }

    public function down()
    {
        $this->dropTable('{{%sirkuler}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200316_041223_sirkuler cannot be reverted.\n";

        return false;
    }
    */
}
