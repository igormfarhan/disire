<?php

use yii\db\Migration;

/**
 * Class m200318_074115_add_fk_submit_user
 */
class m200318_074115_add_fk_submit_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_submit_user', '{{%sirkuler}}', 'submit_user', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200318_074115_add_fk_submit_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200318_074115_add_fk_submit_user cannot be reverted.\n";

        return false;
    }
    */
}
