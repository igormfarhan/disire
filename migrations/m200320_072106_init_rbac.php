<?php

use yii\db\Migration;

/**
 * Class m200320_072106_init_rbac
 */
class m200320_072106_init_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        // add "createPost" permission
        $createKontrak = $auth->createPermission('createKontrak');
        $createKontrak->description = 'Create a kontrak';
        $auth->add($createKontrak);

        // add "updatePost" permission
        $updateKontrak = $auth->createPermission('updateKontrak');
        $updateKontrak->description = 'Update kontrak';
        $auth->add($updateKontrak);

        // add "author" role and give this role the "createPost" permission
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $createKontrak);

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updateKontrak);
        $auth->addChild($admin, $user);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($author, 2);
        $auth->assign($admin, 1);
    }
    
    public function down()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();
    }
    /**
     * {@inheritdoc}
     */
    // public function safeUp()
    // {

    // }

    // /**
    //  * {@inheritdoc}
    //  */
    // public function safeDown()
    // {
    //     echo "m200320_072106_init_rbac cannot be reverted.\n";

    //     return false;
    // }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200320_072106_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
