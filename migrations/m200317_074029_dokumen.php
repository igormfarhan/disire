<?php

use yii\db\Migration;

/**
 * Class m200317_074029_dokumen
 */
class m200317_074029_dokumen extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sirkuler', 'pks',$this->string(255)->notNull());
        $this->addColumn('sirkuler', 'pks_stat',$this->smallInteger()->notNull()->defaultValue(0));
        $this->addColumn('sirkuler', 'akta_pendirian',$this->string(255)->notNull());
        $this->addColumn('sirkuler', 'akta_pendirian_stat',$this->smallInteger()->notNull()->defaultValue(0));
        $this->addColumn('sirkuler', 'akta_terbaru',$this->string(255)->notNull());
        $this->addColumn('sirkuler', 'akta_terbaru_stat',$this->smallInteger()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200317_074029_dokumen cannot be reverted.\n";
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200317_074029_dokumen cannot be reverted.\n";

        return false;
    }
    */
}
