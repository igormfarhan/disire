<?php

use yii\db\Migration;

/**
 * Class m200317_031708_drop_submitTime
 */
class m200317_031708_drop_submitTime extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%sirkuler}}', 'submit_time');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200317_031708_drop_submitTime cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200317_031708_drop_submitTime cannot be reverted.\n";

        return false;
    }
    */
}
