var i = 1;

$('.progress2 .circle2').removeClass().addClass('circle2');
$('.progress2 .bar2').removeClass().addClass('bar2');
setInterval(function() {
  $('.progress2 .circle2:nth-of-type(' + i + ')').addClass('active');
  
  $('.progress2 .circle2:nth-of-type(' + (i-1) + ')').removeClass('active').addClass('done');
  
  $('.progress2 .circle2:nth-of-type(' + (i-1) + ') .label2').html('&#10003;');
  
  $('.progress2 .bar2:nth-of-type(' + (i-1) + ')').addClass('active');
  
  $('.progress2 .bar2:nth-of-type(' + (i-2) + ')').removeClass('active').addClass('done');
  
  i++;
  
  if (i==0) {
    $('.progress2 .bar2').removeClass().addClass('bar2');
    $('.progress2 div.circle2').removeClass().addClass('circle2');
    i = 1;
  }
}, 1000);